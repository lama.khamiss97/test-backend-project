<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {

        $users = User::all()->where('roles_id', 2);
        return view('Admin.Users.Users', compact('users'));
    }

    public function AddUser(Request $request)
    {
        $user = $this->create($request);
        return back();
    }

    public function EditUser($id, Request $request)
    {

        $user = $this->edit($id,
            $request);
        return back();
    }

    public function Registration(Request $request)
    {
        $user = $this->create($request);
        return view('User.product.product');
    }
    public function profile()
    {
        $user = User::query()->findOrFail(auth()->user()->id);
        return view('profile',compact('user'));
    }
    public function delete($id)
    {
        $user = $this->deleteuser($id);

        return back();
    }
    //*API*//
    public function profileApi($id)
    {
        $user = User::query()->findOrFail($id);
        return response()->json($user);
    }


    public function users()
    {
        $users = User::all()->where('roles_id', 2);
        return response()->json($users);
    }


    public function create( Request $request)
    {
        $user = User::create([
            'First_Name' => $request['First_Name'],
            'Last_Name' => $request['Last_Name'],
            'email' => $request['email'],
            'Phone_Number' => $request['Phone_Number'],
            'password' => Hash::make($request['password']),
            'roles_id' => 2,
        ]);
        return response()->json($user);
    }

    public function edit($id, Request $request)
    {

        $user = User::query()->findOrFail($id);
        $user->Last_Name = $request->Last_Name;
        if ($request->password != null) $user->password = Hash::make($request->password);
        $user->First_Name = $request->First_Name;
        $user->email = $request->email;
        $user->Phone_Number = $request->Phone_Number;
        $user->save();
        return response()->json($user);
    }


    public function deleteuser($id)
    {
        $user = User::query()->findOrFail($id);
        $user->delete();
        return response()->json($user);
    }

    public function Login(Request $request)
    {


        if (filter_var($request->login_id, FILTER_VALIDATE_EMAIL)) {
            $user_id = 'email';
            $user = User::Where('email', $request->login_id)->first();

        } elseif (filter_var((int)$request->login_id, FILTER_VALIDATE_INT)) {
            $user_id = 'Phone_Number';

            $user = User::Where('Phone_Number', (int)$request->login_id)->first();

        }
        if ($user == null )
            return response( )->json('incorrect password or username, please make sure to enter them correctly.');


        return $user;




    }
}
