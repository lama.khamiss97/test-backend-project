<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::query()->where('user_id', auth()->id())->paginate(2);
        return view('User.product.product', compact('products'));
    }

    public function viewProduct($id)
    {


        $products = Product::query()->where('user_id', $id)->paginate(2);
        $users = User::all()->where('roles_id', 2);

        return view('User.product.product', compact('products', 'users'));
    }

    public function AllProduct()
    {


        $products = Product::query()->paginate(2);
        $users = User::all()->where('roles_id', 2);

        return view('User.product.product', compact('products', 'users'));
    }

    public function create(Request $request)
    {
        if ($request->imageUrl != null) {
            $imageName = 'content_' . time() . $request->imageUrl->getClientOriginalName();
            $imagePath = '/uploads/' . $imageName;
            $request->imageUrl->move(public_path('uploads'), $imageName);
        }
        Product::create([
            'name' => $request['name'],
            'Description' => $request['description'],
            'Image' => $imagePath,
            'user_id' => auth()->id(),
        ]);
        return back();
    }

    public function edit($id, Request $request)
    {


        $product = Product::query()->findOrFail($id);

        if ($request->user_id != null) $product->user_id = $request->user_id;
        $product->name = $request->name;
        $product->Description = $request->description;

        $product->save();

        return back();
    }

    public function delete($id)
    {
        $product = Product::query()->findOrFail($id);
        $product->delete();
        return back();
    }
/*API*/
    public function ProductByUserId($user_id)
    {
        $products = Product::query()->where('user_id', $user_id)->paginate(3);
        return response()->json($products);
    }


    public function ProductByID($id)
    {


        $product = Product::query()->findOrFail($id);

        return response()->json($product);
    }

    public function Products()
    {

       $products = Product::query()->paginate(3);

        return response()->json($products);
    }

    public function createProduct(Request $request)
    {
        if ($request->imageUrl != null) {
            $imageName = 'content_' . time() . $request->imageUrl->getClientOriginalName();
            $imagePath = '/uploads/' . $imageName;
            $request->imageUrl->move(public_path('uploads'), $imageName);
        }
        $product = Product::create([
            'name' => $request['name'],
            'Description' => $request['description'],
            'Image' => $imagePath,
            'user_id' => $request['user_id'] ,
        ]);
        return response()->json($product);
    }

    public function editProduct($id, Request $request)
    {


        $product = Product::query()->findOrFail($id);

        if ($request->user_id != null) $product->user_id = $request->user_id;
        $product->name = $request->name;
        $product->Description = $request->description;

        $product->save();

        return response()->json($product);
    }

    public function deleteProduct($id)
    {
        $product = Product::query()->findOrFail($id);
        $product->delete();
        return response()->json($product);
    }
}
