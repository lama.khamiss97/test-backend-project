<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::table('users',function (Blueprint $table) {
            $table->unsignedBigInteger('roles_id')->after('id')->nullable();
            $table->foreign('roles_id')->references('id')
                ->on('roles')->onUpdate('cascade')->onDelete('cascade');
        });
        DB::table('roles')->insert(['name' => "Admin"] );
        DB::table('roles')->insert(['name' => "User"]);
        DB::table('users')->insert(['First_Name' => "Admin",'Last_Name' => "Admin",'email'=>"Admin@Admin.com",'password'=>Hash::make('123456789'),'roles_id'=>1]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
