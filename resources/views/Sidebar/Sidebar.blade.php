<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('Sidebar/Sidebar.css') }}" rel="stylesheet">

</head>

<header class="header" role="banner">
    <h1 class="logo">
        <a href="{{ '/logout'}}"><span>{{auth()->user()->First_Name}}</span></a>
    </h1>
    <div class="nav-wrap">
        <nav class="main-nav" role="navigation">
            <ul class="unstyled list-hover-slide">

                @if(auth()->user()->roles_id == 1)
                    <li><a href="{{route('users')}}">Users</a></li>
                    <li><a href="{{route('AllProduct')}}">Products</a></li>

                @elseif(auth()->user()->roles_id == 2)
                    <li><a href="{{route('Product')}}">Products</a></li>
                @endif
                <li><a href="{{route('profile')}}">profile</a></li>

                <li><a href="{{'/logout'}}">Logout</a></li>
            </ul>
        </nav>

    </div>
</header>


</html>
