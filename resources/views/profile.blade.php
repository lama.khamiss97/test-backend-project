<!DOCTYPE html>
<html>
@include('Sidebar.Sidebar')

<head>

    <link rel="stylesheet" href="{{asset('Admin/Admin.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>


<div style="margin-left: 250px" class="container">
    <h3>profile</h3>

    <table class="table table-bordered">
        <tr>

            <th>First Name</th>
            <th>Last Name</th>

            <th>user email</th>
            <th>Phone Number</th>
            <th width="100px">Action</th>
        </tr>

                <tr id="tr_{{$user->id}}">
                    <td>{{ $user->First_Name }}</td>
                    <td>{{ $user->Last_Name }}</td>

                    <td>{{ $user->email }}</td>
                    <td>{{ $user->Phone_Number }}</td>
                    <td>

                        <button class="btn btn-info" data-id="{{$user->id}}"
                                data-toggle="modal"
                                data-target="#ModalEdit_{{$user->id}}">Edit  information
                        </button>
                    </td>
                </tr>
                <div class="modal fade ModalEditCity" id="ModalEdit_{{$user->id}}"
                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{route('edit', $user->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ModalEditLabel">Edit </h5>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="First_Name" class="col-form-label">First Name</label>
                                        <input required name="First_Name" type="text" class="form-control"
                                               id="First_Name"
                                               value="{{$user->First_Name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Last_Name" class="col-form-label">Last Name</label>
                                        <input required name="Last_Name" type="text" class="form-control" id="Last_Name"
                                               value="{{$user->Last_Name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email " class="col-form-label"> Email </label>
                                        <input required name="email" type="email" class="form-control" id="email"
                                               value="{{$user->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone_Number" class="col-form-label">Phone Number</label>
                                        <input required name="Phone_Number" type="number" class="form-control"
                                               id="Phone_Number"
                                               value="{{$user->Phone_Number}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-form-label">Password</label>
                                        <input name="password" type="password" class="form-control" id="password">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-primary">Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


    </table>
</div> <!-- container / end -->

</body>


<script src="{{asset('Admin/Admin.js')}}"></script>
<script>

    $('.imageUrl').change(function () {
        let image = $($(this).data('target')),
            file = this.files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function () {
            image.attr('src', reader.result);
        }, false);
        if (file) {
            reader.readAsDataURL(file);
        }

    });

</script>
</html>
