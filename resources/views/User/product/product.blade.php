<!DOCTYPE html>
<html>
@include('Sidebar.Sidebar')

<head>
    <style>
        .display_image {
            width: 400px;
            height: 225px;
            border: 1px solid black;
            background-position: center;
            background-size: cover;
        }
    </style>

    <link rel="stylesheet" href="{{asset('Admin/Admin.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>


<div style="margin-left: 250px" class="container">
    <h3>Products</h3>
    <div style="margin-bottom: 20px">
@if(auth()->user()->roles_id == 2)
        <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#ModalAdd" data-whatever="@getbootstrap">Add Product
        </button>
@endif
    </div>
    <table class="table table-bordered">
        <tr>


            <th>Product Name</th>
            <th>Product Description</th>
            <th width="200px">Action</th>
        </tr>
        @if($products->count())
            @foreach($products as $key => $product)
                <tr id="tr_{{$product->id}}">

                    <td>{{ $product->name }}</td>
                    <td>{{ $product->Description }}</td>

                    <td>

                        <form class="mr-1 " action="{{route('deleteProduct', $product->id)}}"
                              method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger delete_button" type="submit">
                                Delete
                            </button>
                        </form>
                        <button class="btn btn-info" data-id="{{$product->id}}"
                                data-toggle="modal"
                                data-target="#ModalEdit_{{$product->id}}">Edit Product
                        </button>


                    </td>
                </tr>
                <div class="modal fade ModalEditCity" id="ModalEdit_{{$product->id}}"
                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{route('editProduct', $product->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ModalEditLabel">Edit Product</h5>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @if(auth()->user()->roles_id == 1)
                                    <div class="form-group">
                                        <label for="description " class="col-form-label"> Description </label>
                                        <select  name="user_id" id="user"
                                                class="form-control">

                                            <option value="">Select users </option>
                                            @foreach($users as $user)
                                                <option
                                                    value="{{$user->id}}">{{$user->First_Name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name" class="col-form-label">Name</label>
                                        <input required name="name" type="text" class="form-control" id="name"
                                               value="{{$product->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description " class="col-form-label"> Description </label>
                                        <input name="description" type="text" class="form-control" id="description"
                                               value="{{$product->Description}}">
                                    </div>

                                                                        <div class="form-group">
                                                                            <label for="image_edit_{{$product->id}}"
                                                                                   class="col-form-label">
                                                                                @if($product->imageUrl)
                                                                                    <img style="width: 100px"
                                                                                         src="{{asset($product->imageUrl)}}" alt=""
                                                                                         class="image_edit_{{$product->id}}">
                                                                                @else
                                                                                    <img style="width: 100px"
                                                                                         src="{{asset('assets/admin/images/city.png')}}"
                                                                                         alt=""
                                                                                         class="image_edit_{{$product->id}}">
                                                                                @endif
                                                                            </label>
                                                                            <input data-target="image_edit_{{$product->id}}"
                                                                                   name="imageUrl" type="file"
                                                                                   class="form-control imageUrl d-none"
                                                                                   id="image_edit_{{$product->id}}">
                                                                            @error('imageUrl')
                                                                            <span class="text-danger">{{$message}}</span>
                                                                            @enderror
                                                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-primary">Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @endforeach
        @endif
    </table>
    <div class="d-flex justify-content-center">
        {!! $products->links() !!}
    </div>
</div> <!-- container / end -->

<div class="modal fade add_modal" id="ModalAdd" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{route('createProduct')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalAddLabel">New Product</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Name</label>
                        <input required name="name" type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="description " class="col-form-label"> Description </label>
                        <input name="description" type="text" class="form-control" id="description">
                    </div>

                    <div class="form-group">
                        <label for="add_image" class="col-form-label">

                            <img class=" avatar main" src="{{asset('img/plus.png')}}"
                                 alt="avatar" style="width: 100px;">

                        </label>
                        <input hidden data-target=".add_image" name="imageUrl" type="file"
                               class="form-control" id="add_image">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>


</body>


<script src="{{asset('Admin/Admin.js')}}"></script>
</html>
