<head>

    <link href="{{ asset('Auth/login.css') }}" rel="stylesheet">
</head>


<div class="login-box">
    <h2>Registration</h2>
    <form class="login-container" method="POST" action="{{ route('Registration') }}">
        @csrf

        <div class="user-box">
            <input placeholder="First Name" id="First_Name" type="text"
                   class="form-control @error('First_Name') is-invalid @enderror"
                   name="First_Name"
                   value="{{ old('First_Name') }}" required autocomplete="email" autofocus>

            @error('First_Name')
            <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                       </span>
            @enderror
            <label>First Name</label>
        </div>
        <div class="user-box">
            <input placeholder="Last Name" id="Last_Name" type="text"
                   class="form-control @error('Last_Name') is-invalid @enderror"
                   name="Last_Name"
                   value="{{ old('Last_Name') }}" required autocomplete="Last_Name" autofocus>

            @error('Last_Name')
            <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                       </span>
            @enderror
            <label>Last Name </label>
        </div>
        <div class="user-box">
            <input placeholder="Email " id="email" type="email"
                   class="form-control @error('email') is-invalid @enderror"
                   name="email"
                   value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                       </span>
            @enderror
            <label>Email</label>
        </div>
        <div class="user-box">
            <input placeholder="Phone Number" id="Phone_Number" type="number"
                   class="form-control @error('Phone_Number') is-invalid @enderror"
                   name="Phone_Number"
                   value="{{ old('Phone_Number') }}" required autocomplete="Phone_Number" autofocus>

            @error('Phone_Number')
            <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                       </span>
            @enderror
            <label>Phone Number </label>
        </div>
        <div class="user-box">
            <input placeholder="Password" id="password" type="password" --}}
                   class="form-control @error('password') is-invalid @enderror" name="password"
                   required
                   autocomplete="current-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
                                                                              <strong>{{ $message }}</strong>
                                                                          </span>
            @enderror
            <label>Password</label>
        </div>
        <button type="submit" value="Log in">

            Submit
        </button>
    </form>


</div>
