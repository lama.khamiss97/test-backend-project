<!DOCTYPE html>
<html>
@include('Sidebar.Sidebar')

<head>

    <style>
        .display_image {
            width: 400px;
            height: 225px;
            border: 1px solid black;
            background-position: center;
            background-size: cover;
        }
    </style>
    <link rel="stylesheet" href="{{asset('Admin/Admin.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>


<div style="margin-left: 250px" class="container">
    <h3>Users</h3>
    <div style="margin-bottom: 20px">

        <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#ModalAdd" data-whatever="@getbootstrap">Add User
        </button>

    </div>
    <table class="table table-bordered">
        <tr>
            {{--            <th width="50px"><input type="checkbox" id="master"></th>--}}

            <th>First Name</th>
            <th>Last Name</th>

            <th>user email</th>
            <th>Phone Number</th>
            <th width="100px">Action</th>
        </tr>
        @if($users->count())
            @foreach($users as $key => $user)
                <tr id="tr_{{$user->id}}">
                    {{--                    <td><input type="checkbox" class="sub_chk" data-id="{{$user->id}}"></td>--}}

                    <td>{{ $user->First_Name }}</td>
                    <td>{{ $user->Last_Name }}</td>

                    <td>{{ $user->email }}</td>
                    <td>{{ $user->Phone_Number }}</td>
                    <td>


                        <form class="mr-1 " action="{{route('delete', $user->id)}}"
                              method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger delete_button" type="submit">
                                Delete
                            </button>
                        </form>
                        <button class="btn btn-info" data-id="{{$user->id}}"
                                data-toggle="modal"
                                data-target="#ModalEdit_{{$user->id}}">Edit user
                        </button>
                        <a href="{{ url('viewProduct',$user->id) }}" class="btn btn-group-vertical btn-sm">

                            view Product
                        </a>
                    </td>
                </tr>
                <div class="modal fade ModalEditCity" id="ModalEdit_{{$user->id}}"
                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{route('edit', $user->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ModalEditLabel">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="First_Name" class="col-form-label">First Name</label>
                                        <input required name="First_Name" type="text" class="form-control"
                                               id="First_Name"
                                               value="{{$user->First_Name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Last_Name" class="col-form-label">Last Name</label>
                                        <input required name="Last_Name" type="text" class="form-control" id="Last_Name"
                                               value="{{$user->Last_Name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email " class="col-form-label"> Email </label>
                                        <input required name="email" type="email" class="form-control" id="email"
                                               value="{{$user->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone_Number" class="col-form-label">Phone Number</label>
                                        <input required name="Phone_Number" type="number" class="form-control"
                                               id="Phone_Number"
                                               value="{{$user->Phone_Number}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-form-label">Password</label>
                                        <input name="password" type="password" class="form-control" id="password">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-primary">Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @endforeach
        @endif
    </table>
</div> <!-- container / end -->

<div class="modal fade add_modal" id="ModalAdd" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{route('create')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalAddLabel">New User</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="First_Name" class="col-form-label">First Name</label>
                        <input required name="First_Name" type="text" class="form-control" id="First_Name">
                    </div>
                    <div class="form-group">
                        <label for="Last_Name " class="col-form-label"> Last Name </label>
                        <input required name="Last_Name" type="text" class="form-control" id="Last_Name">
                    </div>
                    <div class="form-group">
                        <label for="email " class="col-form-label"> Email </label>
                        <input required name="email" type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="Phone_Number " class="col-form-label"> Phone Number </label>
                        <input required name="Phone_Number" type="number" class="form-control" id="Phone_Number">
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password</label>
                        <input required name="password" type="password" class="form-control" id="password">
                    </div>


                    {{--                    <div class="form-group">--}}

                    {{--                        <label for="type_image" class="col-form-label">--}}
                    {{--                            Image--}}
                    {{--                            <img style="width: 100px" src="{{asset('img/plus.png')}}" alt=""--}}
                    {{--                                 class="type_image">--}}
                    {{--                        </label>--}}
                    {{--                        <input  name="imageUrl" data-target=".type_image" type="file"--}}
                    {{--                               class="form-control imageUrl d-none" id="type_image">--}}
                    {{--                    </div>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>


<script src="{{asset('Admin/Admin.js')}}"></script>
<script>

    $('.imageUrl').change(function () {
        let image = $($(this).data('target')),
            file = this.files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function () {
            image.attr('src', reader.result);
        }, false);
        if (file) {
            reader.readAsDataURL(file);
        }

    });

</script>
</html>
