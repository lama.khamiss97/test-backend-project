<head>
    <link href="{{ asset('Auth/login.css') }}" rel="stylesheet">
</head>
{{--<div class="login">--}}
{{--    <div class="login-triangle"></div>--}}

{{--    <h2 class="login-header">Log in </h2>--}}
{{--    <form class="login-container" method="POST" action="{{ route('CheckloginAdmin') }}">--}}
{{--        @csrf--}}


{{--        <p><input placeholder="Email Or Phone Number" id="email" type="email" class="form-control @error('email') is-invalid @enderror"--}}
{{--                  name="email"--}}
{{--                  value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--            @error('email')--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--            @enderror</p>--}}
{{--        <p><input placeholder="Password" id="password" type="password"--}}
{{--                  class="form-control @error('password') is-invalid @enderror" name="password" required--}}
{{--                  autocomplete="current-password">--}}

{{--            @error('password')--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--            @enderror</p>--}}

{{--        --}}
{{--        <p><input type="submit" value="Log in"></p>--}}
{{--    </form>--}}
{{--</div>--}}
<div class="login-box">
    <h2>Login</h2>
    <form class="login-container" method="POST" action="{{ route('CheckloginAdmin') }}">
        @csrf

        <div class="user-box">
            <input placeholder="Email Or Phone Number" id="email" type="text"
                   class="form-control @error('email') is-invalid @enderror"
                   name="email"
                   value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                       </span>
            @enderror
            <label>Email Or Phone Number </label>
        </div>
        <div class="user-box">
            <input placeholder="Password" id="password" type="password" --}}
                   class="form-control @error('password') is-invalid @enderror" name="password"
                   required
                   autocomplete="current-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
                                                                              <strong>{{ $message }}</strong>
                                                                          </span>
            @enderror
            <label>Password</label>
        </div>
        <button type="submit" value="Log in">

            Submit
        </button>
    </form>

    <a href="/Registrater">Registrater with email</a>
</div>
