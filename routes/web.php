<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\User\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Registrater', function () {
    return view('Registration');
});

Route::get('/', [LoginController::class, 'showLoginAdmin']);
Route::get('/loginSupervisor', [LoginController::class, 'showLoginSupervisor']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::prefix('Admin')->group(function () {
    Route::post('/login', [LoginController::class, 'loginAdmin'])->name('CheckloginAdmin');
    Route::get('/users', [UserController::class, 'index'])->name('users');
    Route::post('/create', [UserController::class, 'AddUser'])->name('create');
    Route::patch('/edit/{id}', [UserController::class, 'EditUser'])->name('edit');
    Route::delete('/delete/{id}', [UserController::class, 'delete'])->name('delete');

});

Route::prefix('User')->group(function () {

    Route::get('/Product', [ProductController::class, 'index'])->name('Product');

    Route::post('/createProduct', [ProductController::class, 'create'])->name('createProduct');
    Route::patch('/editProduct/{id}', [ProductController::class, 'edit'])->name('editProduct');
    Route::delete('/deleteProduct/{id}', [ProductController::class, 'delete'])->name('deleteProduct');
});
Route::post('/Registration', [UserController::class, 'Registration'])->name('Registration');
Route::get('/profile', [UserController::class, 'profile'])->name('profile');
Route::get('/viewProduct/{id}', [ProductController::class, 'viewProduct'])->name('viewProduct');
Route::get('/AllProduct', [ProductController::class, 'AllProduct'])->name('AllProduct');
//Auth::routes(['login' => false,'register' => false]);
//Route::get('/home', [LoginController::class, 'logout'])->name('home');
