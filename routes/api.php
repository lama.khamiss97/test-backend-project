<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\User\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/Login', [UserController::class, 'Login']);


    Route::get('/users', [UserController::class, 'users']);
    Route::post('/create', [UserController::class, 'create']);
    Route::post('/edit/{id}', [UserController::class, 'edit']);
    Route::delete('/delete/{id}', [UserController::class, 'deleteuser']);
    Route::get('/profileApi/{id}', [UserController::class, 'profileApi']);

    Route::get('/Products', [ProductController::class, 'Products']);
    Route::get('/ProductByID/{id}', [ProductController::class, 'ProductByID']);
    Route::get('/ProductByUserId/{user_id}', [ProductController::class, 'ProductByUserId']);
    Route::post('/createProduct', [ProductController::class, 'createProduct']);
    Route::post('/editProduct/{id}', [ProductController::class, 'editProduct']);
    Route::delete('/deleteProduct/{id}', [ProductController::class, 'deleteProduct']);

